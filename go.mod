module go.jolheiser.com/go-slash

go 1.16

require (
	github.com/go-chi/chi/v5 v5.0.2
	go.jolheiser.com/disco v0.0.3
)
