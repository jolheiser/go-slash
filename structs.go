package slash

import "go.jolheiser.com/disco"

// CreateApplicationCommand https://discord.com/developers/docs/interactions/slash-commands#applicationcommand
type CreateApplicationCommand struct {
	Name        string                      `json:"name"`
	Description string                      `json:"description"`
	Options     []*ApplicationCommandOption `json:"options,omitempty"`
}

// ApplicationCommand https://discord.com/developers/docs/interactions/slash-commands#applicationcommand
type ApplicationCommand struct {
	ID            string `json:"id"`
	ApplicationID string `json:"application_id"`
	CreateApplicationCommand
}

// ApplicationCommandOptionType (ACOT)
type ApplicationCommandOptionType int

// https://discord.com/developers/docs/interactions/slash-commands#applicationcommandoptiontype
const (
	SubCommandACOT ApplicationCommandOptionType = iota + 1
	SubCommandGroupACOT
	StringACOT
	IntegerACOT
	BooleanACOT
	UserACOT
	ChannelACOT
	RoleACOT
)

// ApplicationCommandOption https://discord.com/developers/docs/interactions/slash-commands#applicationcommandoption
type ApplicationCommandOption struct {
	Type        ApplicationCommandOptionType      `json:"type"`
	Name        string                            `json:"name"`
	Description string                            `json:"description"`
	Required    bool                              `json:"required,omitempty"`
	Choices     []*ApplicationCommandOptionChoice `json:"choices,omitempty"`
	Options     []*ApplicationCommandOption       `json:"options,omitempty"`
}

// ApplicationCommandOptionChoice https://discord.com/developers/docs/interactions/slash-commands#applicationcommandoptionchoice
type ApplicationCommandOptionChoice struct {
	Name  string      `json:"name"`
	Value interface{} `json:"value"`
}

// InteractionType (IT)
type InteractionType int

// https://discord.com/developers/docs/interactions/slash-commands#interaction-interactiontype
const (
	PingIT InteractionType = iota + 1
	ApplicationCommandIT
)

// https://discord.com/developers/docs/interactions/slash-commands#interaction
type Interaction struct {
	ID        string                             `json:"id"`
	Type      InteractionType                    `json:"type"`
	Data      *ApplicationCommandInteractionData `json:"data"`
	GuildID   string                             `json:"guild_id"`
	ChannelID string                             `json:"channel_id"`
	Member    *Member                            `json:"member"`
	User      *User                              `json:"user"`
	Token     string                             `json:"token"`
	Version   int                                `json:"version"`
}

// ApplicationCommandInteractionData https://discord.com/developers/docs/interactions/slash-commands#interaction-applicationcommandinteractiondata
type ApplicationCommandInteractionData struct {
	ID      string                                     `json:"id"`
	Name    string                                     `json:"name"`
	Options []*ApplicationCommandInteractionDataOption `json:"options"`
}

// ApplicationCommandInteractionDataOption https://discord.com/developers/docs/interactions/slash-commands#interaction-applicationcommandinteractiondataoption
type ApplicationCommandInteractionDataOption struct {
	Name    string                                     `json:"name"`
	Value   interface{}                                `json:"value"`
	Options []*ApplicationCommandInteractionDataOption `json:"options"`
}

// ValueInt returns the int (forced from JSON float64) or 0
func (o ApplicationCommandInteractionDataOption) ValueInt() int {
	if f, ok := o.Value.(float64); ok {
		return int(f)
	}
	return 0
}

// ValueString returns the string or ""
func (o ApplicationCommandInteractionDataOption) ValueString() string {
	if s, ok := o.Value.(string); ok {
		return s
	}
	return ""
}

// ValueBool returns the boolean or false
func (o ApplicationCommandInteractionDataOption) ValueBool() bool {
	if b, ok := o.Value.(bool); ok {
		return b
	}
	return false
}

// Member https://discord.com/developers/docs/resources/guild#guild-member-object
type Member struct {
	User         *User    `json:"user"`
	Nick         string   `json:"nick"`
	Roles        []string `json:"roles"`
	JoinedAt     string   `json:"joined_at"`
	PremiumSince string   `json:"premium_since"`
	Deaf         bool     `json:"deaf"`
	Mute         bool     `json:"mute"`
	Pending      bool     `json:"pending"`
	Permissions  string   `json:"permissions"`
}

// PremiumType (PT)
type PremiumType int

// https://discord.com/developers/docs/resources/user#user-object-premium-types
const (
	NonePT PremiumType = iota
	NitroClassic
	Nitro
)

// UserFlags (UF)
type UserFlags int

// https://discord.com/developers/docs/resources/user#user-object-user-flags
const (
	NoneUF            UserFlags = 0
	DiscordEmployeeUF UserFlags = 1 << iota
	PartneredServerOwnerUF
	HypeSquadEventsUF
	BugHunterLevel1UF
	_
	_
	HouseBraveryUF
	HouseBrillianceUF
	HouseBalanceUF
	EarlySupporterUF
	TeamUserUF
	_
	SystemUF
	_
	BugHunterLevel2UF
	_
	VerifiedBotUF
	EarlyVerifiedBotDeveloperUF
)

// Has checks for a specific UserFlags
func (u UserFlags) Has(f UserFlags) bool {
	return u&f != 0
}

// User https://discord.com/developers/docs/resources/user#user-object
type User struct {
	ID            string      `json:"id"`
	Username      string      `json:"username"`
	Discriminator string      `json:"discriminator"`
	Avatar        string      `json:"avatar"`
	Bot           bool        `json:"bot"`
	System        bool        `json:"system"`
	MFAEnabled    bool        `json:"mfa_enabled"`
	Locale        string      `json:"locale"`
	Verified      bool        `json:"verified"`
	Email         string      `json:"email"`
	Flags         UserFlags   `json:"flags"`
	PremiumType   PremiumType `json:"premium_type"`
	PublicFlags   UserFlags   `json:"public_flags"`
}

// InteractionResponseType (IRT)
type InteractionResponseType int

// https://discord.com/developers/docs/interactions/slash-commands#interaction-response-interactionresponsetype
const (
	PongIRT InteractionResponseType = iota + 1
	_
	_
	ChannelMessageWithSourceIRT
	DeferredChannelMessageWithSourceIRT
)

// InteractionResponse https://discord.com/developers/docs/interactions/slash-commands#interaction-response
type InteractionResponse struct {
	Type InteractionResponseType                    `json:"type"`
	Data *InteractionApplicationCommandCallbackData `json:"data"`
}

// CallbackFlags (CF)
type CallbackFlags int

const (
	NoneCF      CallbackFlags = 0
	EphemeralCF CallbackFlags = 64
)

// InteractionApplicationCommandCallbackData https://discord.com/developers/docs/interactions/slash-commands#interaction-response-interactionapplicationcommandcallbackdata
type InteractionApplicationCommandCallbackData struct {
	TTS             bool                   `json:"tts"`
	Content         string                 `json:"content"`
	Embeds          []*disco.Embed         `json:"embeds"`
	AllowedMentions *disco.AllowedMentions `json:"allowed_mentions"`
	Flags           CallbackFlags          `json:"flags"`
}

// WebhookEdit https://discord.com/developers/docs/resources/webhook#edit-webhook-message
type WebhookEdit struct {
	Content         string                 `json:"content"`
	Embeds          []*disco.Embed         `json:"embeds"`
	AllowedMentions *disco.AllowedMentions `json:"allowed_mentions"`
}

// Command is used when creating a Handler
//
// ID should map to a corresponding ApplicationCommand
// Handle is the func to be called whenever the Handler receives a request for the ApplicationCommand
type Command struct {
	ID     string
	Handle CommandHandleFunc
}

// CommandHandleFunc is a func for Command.Handle
type CommandHandleFunc func(*Interaction) (*InteractionResponse, error)
