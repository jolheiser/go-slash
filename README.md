# slash

A Discord slash commands client and HTTP handler.

Wanting to create a bot? I recommend [discordgo](https://github.com/bwmarrin/discordgo) for that instead.

This library is for non-bot slash commands.

[Examples](_examples)

## License

[MIT](LICENSE)