package slash

import (
	"context"
	"encoding/json"
	"fmt"
	"net/http"
)

// GetGuildApplicationCommands gets all slash commands for a guild
func (c *Client) GetGuildApplicationCommands(ctx context.Context, guildID string) ([]*ApplicationCommand, error) {
	endpoint := baseEndpoint + fmt.Sprintf("applications/%s/guilds/%s/commands", c.clientID, guildID)
	req, err := c.newRequest(ctx, http.MethodGet, endpoint, nil)
	if err != nil {
		return nil, err
	}
	resp, err := c.http.Do(req)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()
	if resp.StatusCode != http.StatusOK {
		return nil, fmt.Errorf("GetGuildApplicationCommands: returned non-200 status code: %s\n%s", resp.Status, errMsg(resp.Body))
	}
	var appCmds []*ApplicationCommand
	return appCmds, json.NewDecoder(resp.Body).Decode(&appCmds)
}

// CreateGuildApplicationCommand creates a slash command for a guild
//
// Creating a guild application command is an upsert, meaning creating a command with the same name will update it
// rather than return an error
func (c *Client) CreateGuildApplicationCommand(ctx context.Context, guildID string, cmd *CreateApplicationCommand) (*ApplicationCommand, error) {
	endpoint := baseEndpoint + fmt.Sprintf("applications/%s/guilds/%s/commands", c.clientID, guildID)
	buf, err := newBuffer(cmd)
	if err != nil {
		return nil, err
	}
	req, err := c.newRequest(ctx, http.MethodPost, endpoint, &buf)
	if err != nil {
		return nil, err
	}
	resp, err := c.http.Do(req)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()
	if resp.StatusCode != http.StatusCreated && resp.StatusCode != http.StatusOK {
		return nil, fmt.Errorf("CreateGuildApplicationCommand: returned non-20x status code: %s\n%s", resp.Status, errMsg(resp.Body))
	}
	var appCmd *ApplicationCommand
	return appCmd, json.NewDecoder(resp.Body).Decode(&appCmd)
}

// BulkOverwriteGuildApplicationCommands bulk overwrites slash commands for a guild
func (c *Client) BulkOverwriteGuildApplicationCommands(ctx context.Context, guildID string, cmd []*CreateApplicationCommand) ([]*ApplicationCommand, error) {
	endpoint := baseEndpoint + fmt.Sprintf("applications/%s/guilds/%s/commands", c.clientID, guildID)
	buf, err := newBuffer(cmd)
	if err != nil {
		return nil, err
	}
	req, err := c.newRequest(ctx, http.MethodPatch, endpoint, &buf)
	if err != nil {
		return nil, err
	}
	resp, err := c.http.Do(req)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()
	if resp.StatusCode != http.StatusCreated && resp.StatusCode != http.StatusOK {
		return nil, fmt.Errorf("CreateGuildApplicationCommand: returned non-20x status code: %s\n%s", resp.Status, errMsg(resp.Body))
	}
	var appCmd []*ApplicationCommand
	return appCmd, json.NewDecoder(resp.Body).Decode(&appCmd)
}

// GetGuildApplicationCommand gets a slash command for a guild by its ID
func (c *Client) GetGuildApplicationCommand(ctx context.Context, guildID, cmdID string) (*ApplicationCommand, error) {
	endpoint := baseEndpoint + fmt.Sprintf("applications/%s/guilds/%s/commands/%s", c.clientID, guildID, cmdID)
	req, err := c.newRequest(ctx, http.MethodGet, endpoint, nil)
	if err != nil {
		return nil, err
	}
	resp, err := c.http.Do(req)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()
	if resp.StatusCode != http.StatusOK {
		return nil, fmt.Errorf("GetGuildApplicationCommand: returned non-200 status code: %s\n%s", resp.Status, errMsg(resp.Body))
	}
	var appCmd *ApplicationCommand
	return appCmd, json.NewDecoder(resp.Body).Decode(&appCmd)
}

// UpdateGuildApplicationCommand updates a slash command for a guild
func (c *Client) UpdateGuildApplicationCommand(ctx context.Context, guildID, cmdID string, cmd *CreateApplicationCommand) (*ApplicationCommand, error) {
	endpoint := baseEndpoint + fmt.Sprintf("applications/%s/guilds/%s/commands/%s", c.clientID, guildID, cmdID)
	buf, err := newBuffer(cmd)
	if err != nil {
		return nil, err
	}
	req, err := c.newRequest(ctx, http.MethodPatch, endpoint, &buf)
	if err != nil {
		return nil, err
	}
	resp, err := c.http.Do(req)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()
	if resp.StatusCode != http.StatusOK {
		return nil, fmt.Errorf("UpdateGuildApplicationCommand: returned non-200 status code: %s\n%s", resp.Status, errMsg(resp.Body))
	}
	var appCmd *ApplicationCommand
	return appCmd, json.NewDecoder(resp.Body).Decode(&appCmd)
}

// DeleteGuildApplicationCommand deletes a slash command for a guild
func (c *Client) DeleteGuildApplicationCommand(ctx context.Context, guildID, cmdID string) error {
	endpoint := baseEndpoint + fmt.Sprintf("applications/%s/guilds/%s/commands/%s", c.clientID, guildID, cmdID)
	req, err := c.newRequest(ctx, http.MethodDelete, endpoint, nil)
	if err != nil {
		return err
	}
	resp, err := c.http.Do(req)
	if err != nil {
		return err
	}
	defer resp.Body.Close()
	if resp.StatusCode != http.StatusNoContent {
		return fmt.Errorf("DeleteGuildApplicationCommand: returned non-204 status code: %s\n%s", resp.Status, errMsg(resp.Body))
	}
	return nil
}
