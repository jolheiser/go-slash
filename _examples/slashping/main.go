package main

import (
	"context"
	"fmt"
	"net/http"
	"os"
	"os/signal"
	"strings"

	"go.jolheiser.com/go-slash"
)

func main() {
	client := slash.NewClient(os.Getenv("SLASH_CLIENT_ID"), os.Getenv("SLASH_CLIENT_SECRET"))
	cmd, err := client.CreateGuildApplicationCommand(context.Background(), os.Getenv("SLASH_GUILD_ID"), &slash.CreateApplicationCommand{
		Name: "ping",
		Description: "PING",
		Options: []*slash.ApplicationCommandOption{
			{
				Name: "num",
				Description: "Number of times to pong",
				Type: slash.IntegerACOT,
				Required: true,
				Choices: []*slash.ApplicationCommandOptionChoice{
					{
						Name: "One",
						Value: 1,
					},
					{
						Name: "Three",
						Value: 3,
					},
					{
						Name: "Five",
						Value: 5,
					},
				},
			},
			{
				Name: "ephemeral",
				Description: "Only you get to see the response",
				Type: slash.BooleanACOT,
			},
		},
	})
	if err != nil {
		fmt.Println(err)
		return
	}

	cmds := []*slash.Command{
		{
			ID: cmd.ID,
			Handle: func(i *slash.Interaction) (*slash.InteractionResponse, error) {
				resp := strings.Repeat("pong", i.Data.Options[0].ValueInt())
				flags := slash.NoneCF
				if len(i.Data.Options) > 1 && i.Data.Options[1].ValueBool() {
					flags = slash.EphemeralCF
				}
				return &slash.InteractionResponse{
					Type: slash.ChannelMessageWithSourceIRT,
					Data: &slash.InteractionApplicationCommandCallbackData{
						Content: resp,
						Flags: flags,
					},
				}, nil
			},
		},
	}
	go func() {
		handler, err := slash.Handler(os.Getenv("SLASH_PUBLIC_KEY"), cmds)
		if err != nil {
			fmt.Println(err)
			return
		}
		fmt.Println("http://localhost:8080")
		if err := http.ListenAndServe(":8080", handler); err != nil {
			fmt.Println(err)
		}
	}()

	ch := make(chan os.Signal)
	signal.Notify(ch, os.Kill, os.Interrupt)
	<-ch
}
