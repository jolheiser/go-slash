package main

import (
	"context"
	"fmt"
	"net/http"
	"os"
	"os/signal"
	"time"

	"go.jolheiser.com/go-slash"
)

func main() {
	client := slash.NewClient(os.Getenv("SLASH_CLIENT_ID"), os.Getenv("SLASH_CLIENT_SECRET"))
	cmd, err := client.CreateGuildApplicationCommand(context.Background(), os.Getenv("SLASH_GUILD_ID"), &slash.CreateApplicationCommand{
		Name: "wait",
		Description: "WAIT",
		Options: []*slash.ApplicationCommandOption{
			{
				Name: "seconds",
				Description: "Number of seconds to wait (max 10) (default 3)",
				Type: slash.IntegerACOT,
			},
		},
	})
	if err != nil {
		fmt.Println(err)
		return
	}

	cmds := []*slash.Command{
		{
			ID: cmd.ID,
			Handle: func(i *slash.Interaction) (*slash.InteractionResponse, error) {
				wait := 3
				if len(i.Data.Options) > 0 {
					wait = i.Data.Options[0].ValueInt()
					if wait > 10 {
						wait = 10
					}
				}
				go func() {
					t := time.NewTimer(time.Second * time.Duration(wait))
					<-t.C
					if err := client.EditInteractionResponse(context.Background(), i.Token, &slash.WebhookEdit{Content: "Time is up!"}); err != nil {
						fmt.Println(err)
					}
				}()
				return &slash.InteractionResponse{
					Type: slash.DeferredChannelMessageWithSourceIRT,
				}, nil
			},
		},
	}
	go func() {
		handler, err := slash.Handler(os.Getenv("SLASH_PUBLIC_KEY"), cmds)
		if err != nil {
			fmt.Println(err)
			return
		}
		fmt.Println("http://localhost:8080")
		if err := http.ListenAndServe(":8080", handler); err != nil {
			fmt.Println(err)
		}
	}()

	ch := make(chan os.Signal)
	signal.Notify(ch, os.Kill, os.Interrupt)
	<-ch
}
