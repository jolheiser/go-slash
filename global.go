package slash

import (
	"context"
	"encoding/json"
	"fmt"
	"net/http"
)

// GetGlobalApplicationCommands gets all global slash commands
func (c *Client) GetGlobalApplicationCommands(ctx context.Context) ([]*ApplicationCommand, error) {
	endpoint := baseEndpoint + fmt.Sprintf("applications/%s/commands", c.clientID)
	req, err := c.newRequest(ctx, http.MethodGet, endpoint, nil)
	if err != nil {
		return nil, err
	}
	resp, err := c.http.Do(req)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()
	if resp.StatusCode != http.StatusOK {
		return nil, fmt.Errorf("GetGlobalApplicationCommands: returned non-200 status code: %s\n%s", resp.Status, errMsg(resp.Body))
	}
	var appCmds []*ApplicationCommand
	return appCmds, json.NewDecoder(resp.Body).Decode(&appCmds)
}

// CreateGlobalApplicationCommand creates a global slash command
//
// Creating a global application command is an upsert, meaning creating a command with the same name will update it
// rather than return an error
func (c *Client) CreateGlobalApplicationCommand(ctx context.Context, cmd *CreateApplicationCommand) (*ApplicationCommand, error) {
	endpoint := baseEndpoint + fmt.Sprintf("applications/%s/commands", c.clientID)
	buf, err := newBuffer(cmd)
	if err != nil {
		return nil, err
	}
	req, err := c.newRequest(ctx, http.MethodPost, endpoint, &buf)
	if err != nil {
		return nil, err
	}
	resp, err := c.http.Do(req)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()
	if resp.StatusCode != http.StatusCreated && resp.StatusCode != http.StatusOK {
		return nil, fmt.Errorf("CreateGlobalApplicationCommand: returned non-20x status code: %s\n%s", resp.Status, errMsg(resp.Body))
	}
	var appCmd *ApplicationCommand
	return appCmd, json.NewDecoder(resp.Body).Decode(&appCmd)
}

// BulkOverwriteGlobalApplicationCommands bulk overwrites global slash commands
func (c *Client) BulkOverwriteGlobalApplicationCommands(ctx context.Context, cmd []*CreateApplicationCommand) ([]*ApplicationCommand, error) {
	endpoint := baseEndpoint + fmt.Sprintf("applications/%s/commands", c.clientID)
	buf, err := newBuffer(cmd)
	if err != nil {
		return nil, err
	}
	req, err := c.newRequest(ctx, http.MethodPatch, endpoint, &buf)
	if err != nil {
		return nil, err
	}
	resp, err := c.http.Do(req)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()
	if resp.StatusCode != http.StatusOK {
		return nil, fmt.Errorf("CreateGlobalApplicationCommand: returned non-200 status code: %s\n%s", resp.Status, errMsg(resp.Body))
	}
	var appCmd []*ApplicationCommand
	return appCmd, json.NewDecoder(resp.Body).Decode(&appCmd)
}

// GetGlobalApplicationCommand gets a global slash command by its ID
func (c *Client) GetGlobalApplicationCommand(ctx context.Context, cmdID string) (*ApplicationCommand, error) {
	endpoint := baseEndpoint + fmt.Sprintf("applications/%s/commands/%s", c.clientID, cmdID)
	req, err := c.newRequest(ctx, http.MethodGet, endpoint, nil)
	if err != nil {
		return nil, err
	}
	resp, err := c.http.Do(req)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()
	if resp.StatusCode != http.StatusOK {
		return nil, fmt.Errorf("GetGlobalApplicationCommand: returned non-200 status code: %s\n%s", resp.Status, errMsg(resp.Body))
	}
	var appCmd *ApplicationCommand
	return appCmd, json.NewDecoder(resp.Body).Decode(&appCmd)
}

// UpdateGlobalApplicationCommand updates a global slash command
func (c *Client) UpdateGlobalApplicationCommand(ctx context.Context, cmdID string, cmd *CreateApplicationCommand) (*ApplicationCommand, error) {
	endpoint := baseEndpoint + fmt.Sprintf("applications/%s/commands/%s", c.clientID, cmdID)
	buf, err := newBuffer(cmd)
	if err != nil {
		return nil, err
	}
	req, err := c.newRequest(ctx, http.MethodPatch, endpoint, &buf)
	if err != nil {
		return nil, err
	}
	resp, err := c.http.Do(req)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()
	if resp.StatusCode != http.StatusOK {
		return nil, fmt.Errorf("UpdateGlobalApplicationCommand: returned non-200 status code: %s\n%s", resp.Status, errMsg(resp.Body))
	}
	var appCmd *ApplicationCommand
	return appCmd, json.NewDecoder(resp.Body).Decode(&appCmd)
}

// DeleteGlobalApplicationCommand deletes a global slash command
func (c *Client) DeleteGlobalApplicationCommand(ctx context.Context, cmdID string) error {
	endpoint := baseEndpoint + fmt.Sprintf("applications/%s/commands/%s", c.clientID, cmdID)
	req, err := c.newRequest(ctx, http.MethodDelete, endpoint, nil)
	if err != nil {
		return err
	}
	resp, err := c.http.Do(req)
	if err != nil {
		return err
	}
	defer resp.Body.Close()
	if resp.StatusCode != http.StatusNoContent {
		return fmt.Errorf("DeleteGlobalApplicationCommand: returned non-204 status code: %s\n%s", resp.Status, errMsg(resp.Body))
	}
	return nil
}
